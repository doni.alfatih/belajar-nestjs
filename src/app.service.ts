import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    const bulan = 'September';
    const tahun = 2022;
    return `Welcome to Batam, ${bulan} ${tahun}`;
  }
}
