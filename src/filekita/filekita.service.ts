import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm'
import { filekita } from './filekita.entity'
import { Repository } from 'typeorm'
import { FilekitaDTO } from './filekita.dto';

@Injectable()
export class FilekitaService {
    constructor(
        @InjectRepository (filekita)
        private filekitaRepository: Repository<filekita>
     ) {}

    async showAll() {
        return await this.filekitaRepository.find();
     }

    async create(data: FilekitaDTO) {
        const filekitaNew = await this.filekitaRepository.create(data);
        await this.filekitaRepository.save(filekitaNew)
        return filekitaNew
    }
    
    /*
    async lihatSemua(){
       return "Ini adalah service";
    }
    */
    async lihatPerRecord(id){
       return await this.filekitaRepository.findOne({where:{id}}) 
    }

    async update(id, data: Partial<FilekitaDTO>){
        await this.filekitaRepository.update({id}, data); //Update data berdasarkan id
        return await this.filekitaRepository.findOne({where:{id}}) 

    }

    async hapusData(id){
        await this.filekitaRepository.delete({id})
        return {deleted:true} //delete true: menampilkan notif berhasil dihapus
    }
}
