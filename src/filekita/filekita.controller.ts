import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
//import { pseudoRandomBytes } from 'crypto';
import { FilekitaService } from './filekita.service';
import { FilekitaDTO } from './filekita.dto';

@Controller('filekita')
export class FilekitaController {
    constructor(private FilekitaService: FilekitaService){ }
    @Get()
    lihatOutput() {
        return this.FilekitaService.showAll(); //Get Data dari Batabase
    }

    @Post()
    membuatRecord(@Body() data: FilekitaDTO ){
        return this.FilekitaService.create(data) //Post Data
    }

    /*
    @Get('service')
    lihatSemua(){
      return this.FilekitaService.lihatSemua();
    }
    */

    @Get(':id')
    lihatDetail(@Param('id') id:string){
      //return 'Ini adalah Controller Detail ' + id;
      return this.FilekitaService.lihatPerRecord(id)
    }

    @Put(':id')
    updateDetail(@Param('id') id:string, @Body() data: Partial<FilekitaDTO>){
      return this.FilekitaService.update(id,data); //update data dengan 2 param (id dan data)
    }

    @Delete(':id')
    menghapusData(@Param('id') id:string){
      return this.FilekitaService.hapusData(id); //hapusData diambil dari file Service
    }

}
