import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FilekitaController } from './filekita.controller';
import { filekita } from './filekita.entity';
import { FilekitaService } from './filekita.service';

@Module({
  imports: [TypeOrmModule.forFeature([filekita])],
  controllers: [FilekitaController],
  providers: [FilekitaService]
})
export class FilekitaModule {}
